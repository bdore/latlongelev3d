# LatLongElev3D

Create a 3D representation of a path based on latitude, longitude and elevation.

The challenge is to translate geodata (latitude, longitude, altitude) to x, y, z coordinates in a 3D plane.

Prototype works with Strava API.

## **[Click to check out the prototype](https://latlng-heroku.herokuapp.com/orbit.html)**

### latlngelev_to_xyz.py

1. Get activity data from API
2. Build dataframe from activity data
	- latlong
	- altitude
	- distance
	- velocity_smooth (not used)
3. Calculate altitude differences
4. Calculate latlng point distances
5. Get bearings (direction) of next point
6. With distance between the points and direction, calculate the position of the next point in the plane.

Final output is a list of Vector3 vertices to be used in a THREE.JS scene.

The example activity path displayed top-down on a map:
![Alt text](https://bitbucket.org/bdore/latlongelev3d/raw/71fba5ecd8aa9f4bcab6204f77f22b3dc4382542/map.png)