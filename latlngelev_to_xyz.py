#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 10:43:53 2018

@author: Bernardo Doré
"""

# LatLongElev3D

import requests
import pandas as pd
import math
from calculate_initial_compass_bearing import calculate_initial_compass_bearing
import sys

def strava_stream_activity(activity_id, access_token):
    activity_id = activity_id
    access_token = access_token
    headers = {'Authorization': 'Bearer ' + access_token}
    activity_url = 'https://www.strava.com/api/v3/activities/' + activity_id + \
    '/streams?keys=latlng,altitude,velocity_smooth,watts,cadence,heartrate&key_by_type=true'
    r = requests.get(activity_url, headers=headers)    
    if r.status_code == 200:
        activity = r.json()
        return activity
    else:
        return

def xy_from_angle_and_length(x1, y1, angle, line_length):
    angle = math.radians(angle)
    new_xy = (x1 + line_length * math.cos(angle), y1 + line_length * math.sin(angle))
    return new_xy

def get_diff(qty_list):
    qty_list = [qty_list[n] - qty_list[n-1] for n in range(1, len(qty_list))]
    qty_list.insert(0, 0.0)
    return qty_list

def main():
    
    #INSERT YOUR STRAVA DEVELOPER TOKEN HERE
    #use mine for testing
    access_token = 'your_token'

    #INSERT ACTIVITY ID
    activity_id = '1461020408'
    #try other activities
    #1338646789
    #1434285737
    #1337595131

    activity = strava_stream_activity(activity_id, access_token)
  
    df_activity_data = pd.DataFrame({'latlng': [tuple(x) for x in activity['latlng']['data']], \
                                    'altitude': activity['altitude']['data'], \
                                    'distance': activity['distance']['data'], \
                                    'velocity_smooth': activity['velocity_smooth']['data']})
    #ALTITUDE DIFF
    df_activity_data['altitude_diff_meters'] = get_diff(df_activity_data.altitude)
    
    #DISTANCE DIFF
    df_activity_data['distance_diff_meters'] = get_diff(df_activity_data.distance)
    
    #BEARINGS IN DEGREES
    bearings = [calculate_initial_compass_bearing(df_activity_data.latlng[n-1], df_activity_data.latlng[n]) \
                for n in range(1, len(df_activity_data.latlng))]
    bearings.insert(0, 0.0)
    
    df_activity_data['bearings'] = bearings
       
    df_bearings_dist_diff = df_activity_data[['bearings', 'distance_diff_meters']]
    
    new_points_list = [(0.0, 0.0)]
    for item in df_bearings_dist_diff.values:
        new_point = xy_from_angle_and_length(new_points_list[-1:][0][0], new_points_list[-1:][0][1],\
                                                         item[0], item[1])
        new_points_list.append(new_point)
    new_points_list.pop(0)

    #START XYZ
    #Y = Altitude
    #Z = Longitude
    #X = Latitude
    df_xyz = pd.DataFrame({'x':[x[0] for x in new_points_list], 'y': [x[1] for x in new_points_list], \
                           'z': df_activity_data['altitude_diff_meters']})
    
    #PRINT TO FILE
    sys.stdout = open('javascript', 'w')
    for item in df_xyz.values:
        z = item[1]
        y = item[2]
        x = item[0]
        q = """geometry.vertices.push(new THREE.Vector3( """ + str(x) + ',' + str(y) + ',' + str(z) + """ ) );"""
        print(q)
    
if __name__ == '__main__':
    main()    